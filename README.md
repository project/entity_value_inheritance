# Entity Value Inheritance (EVI)

The following module was built as a way to sync data between entities. Where
you have a source and a destination. The Inheritance entity is configured to
take into account what source entity, bundle, and field that should be used
and where the destination entity, bundle, and field should map to. Using a
reference field provided on the destination entity to link the source to
the destination.

To get started:

- Download and install the module
- Navigate to /admin/structure/inheritance
- Click Add Inheritance button
- Fill in the form
  - Add Label
  - Ensure Enabled is checked
  - Add Description (optional)
  - Select a Field Strategy
  - Configure Field Strategy (if applicable)
  - Select Source Entity Type
  - Select Source Bundle
  - Select Source Field
  - Select Destination Entity Type
  - Select Destination Bundle
  - Select Destination Field
  - Select Destination Reference Field to Source
- Submit

Known Issues:

- Only a single value entity reference field is allowed for selection currently.
