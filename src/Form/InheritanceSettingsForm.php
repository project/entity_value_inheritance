<?php

namespace Drupal\entity_value_inheritance\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_value_inheritance\Services\Helper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form used to help configure Inheritance settings.
 *
 * @package Drupal\entity_value_inheritance\Form
 */
class InheritanceSettingsForm extends ConfigFormBase {

  /**
   * Helper Service.
   *
   * @var \Drupal\entity_value_inheritance\Services\Helper
   */
  protected Helper $helper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->helper = $container->get('entity_value_inheritance.helper');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'inheritance_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getEditableConfigNames() {
    return ['entity_value_inheritance.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('entity_value_inheritance.settings');

    $form['exclude'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Exclude Configuration'),
      '#description' => $this->t('Exclude specific items from the list of configuration.'),
    ];

    $form['exclude']['field_types'] = [
      '#type' => 'select',
      '#title' => $this->t('Field Types'),
      '#options' => $this->helper->getFieldTypes(),
      '#default_value' => $config->get('exclude')['field_types'] ?? NULL,
      '#description' => $this->t('Exclude specific field types from being able to be copied from source to destination.'),
      '#multiple' => TRUE,
      '#size' => 15,
    ];

    $form['exclude']['entity_types'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Types'),
      '#options' => $this->helper->getEntities(),
      '#default_value' => $config->get('exclude')['entity_types'] ?? NULL,
      '#description' => $this->t('Exclude specific entity types from being able to be referenced.'),
      '#multiple' => TRUE,
      '#size' => 15,
    ];

    $form['exclude']['entity_bundles'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Entity Bundles'),
      '#options' => $this->helper->getEntityBundles(),
      '#default_value' => $config->get('exclude')['entity_bundles'] ?? NULL,
      '#description' => $this->t('Exclude specific entity bundles from being able to be copied from source to destination.'),
      '#size' => 15,
    ];

    $form['exclude']['entity_fields'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Fields'),
      '#options' => $this->helper->getEntityBundleFields(),
      '#default_value' => $config->get('exclude')['entity_fields'] ?? NULL,
      '#description' => $this->t('Exclude specific entity fields from being able to be copied from source to destination.'),
      '#multiple' => TRUE,
      '#size' => 15,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('entity_value_inheritance.settings')
      ->set('exclude', [
        'field_types' => $form_state->getValue('field_types', []),
        'entity_types' => $form_state->getValue('entity_types', []),
        'entity_bundles' => $form_state->getValue('entity_bundles', []),
        'entity_fields' => $form_state->getValue('entity_fields', []),
      ])
      ->save();
    parent::submitForm($form, $form_state);

  }

}
