<?php

namespace Drupal\entity_value_inheritance\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_value_inheritance\Services\Helper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form used for filtering the list.
 */
class InheritanceListBuilderForm extends FormBase {

  /**
   * Entity Type Manager Service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Helper Service.
   *
   * @var \Drupal\entity_value_inheritance\Services\Helper
   */
  protected Helper $helper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->helper = $container->get('entity_value_inheritance.helper');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_value_inheritance_list_builder_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // If reset button clicked redirect to no query params.
    if ($this->getRequest()->query->get('op') === 'Reset') {
      $this->redirect('entity.inheritance.collection')->send();
      return $form;
    }

    $form_state->setMethod('GET');

    $form['source_entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Source Entity'),
      '#options' => $this->getEntityTypes('getSourceEntityType'),
      "#empty_option" => $this->t('- Select -'),
      '#default_value' => $this->getRequest()->query->get('source_entity_type'),
    ];

    $form['source_entity_bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Source Bundles'),
      '#options' => $this->getEntityBundles('getSourceEntityType', 'getSourceBundle'),
      "#empty_option" => $this->t('- Select -'),
      '#default_value' => $this->getRequest()->query->get('source_entity_bundle'),
    ];

    $form['destination_entity_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Destination Entity'),
      '#options' =>  $this->getEntityTypes('getDestinationEntityType'),
      "#empty_option" => $this->t('- Select -'),
      '#default_value' => $this->getRequest()->query->get('destination_entity_type'),
    ];

    $form['destination_entity_bundle'] = [
      '#type' => 'select',
      '#title' => $this->t('Destination Bundle'),
      '#options' => $this->getEntityBundles('getDestinationEntityType', 'getDestinationBundle'),
      "#empty_option" => $this->t('- Select -'),
      '#default_value' => $this->getRequest()->query->get('destination_entity_bundle'),
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
    ];

    if ($this->getRequest()->query->get('op')) {
      $form['actions']['reset'] = [
        '#type' => 'submit',
        '#value' => $this->t('Reset'),
      ];
    }

    $form['#after_build'][] = [get_class($this), 'afterBuild'];

    return $form;
  }

  /**
   * Return a list of available entities.
   *
   * @return string[]
   *   Return array of strings.
   */
  protected function getEntityTypes(string $method): array {
    $list = [];

    foreach ($this->getEntities() as $entity) {
      if (!method_exists($entity, $method)) {
        break;
      }

      if (!isset($list[$entity->$method()])) {
        $list[$entity->$method()] = $this->helper->getEntityTypeLabel($entity->$method());
      }
    }

    asort($list);
    return $list;
  }

  /**
   * Return the entity bundle
   *
   * @param string $entityTypeMethod
   *   Entity Type Method to Call.
   * @param string $method
   *   Bundle Type Method to Call.
   *
   * @return string[][]
   *   Return a list of all bundles.
   */
  protected function getEntityBundles(string $entityTypeMethod, string $method): array {
    $list = [];

    $entityTypes = $this->getEntityTypes($entityTypeMethod);
    foreach ($this->getEntities() as $entity) {
      if (!method_exists($entity, $method)) {
        break;
      }

      $entityTypeLabel = $entityTypes[$entity->$entityTypeMethod()];
      $list[$entityTypeLabel][$entity->$method()] = $this->helper->getEntityBundleLabel($entity->$entityTypeMethod(), $entity->$method());
      asort($list[$entityTypeLabel]);
    }

    asort($list);
    return $list;
  }

  /**
   * Return a list of all inheritance entities.
   *
   * @return \Drupal\entity_value_inheritance\Entity\Inheritance[]
   *   List of Inheritance entities.
   */
  protected function getEntities(): array {
    return $this->entityTypeManager->getStorage('inheritance')->loadMultiple();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * After Build function for removing items.
   */
  public static function afterBuild(array $form, FormStateInterface $form_state) {
    unset($form['form_token']);
    unset($form['form_build_id']);
    unset($form['form_id']);
    return $form;
  }

}
