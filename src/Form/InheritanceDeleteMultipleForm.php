<?php

namespace Drupal\entity_value_inheritance\Form;

use Drupal\Core\Entity\Form\DeleteMultipleForm;

/**
 * Inheritance Delete Multiple Form.
 */
class InheritanceDeleteMultipleForm extends DeleteMultipleForm {}
