<?php

namespace Drupal\entity_value_inheritance;

use Drupal\Component\Plugin\DependentPluginInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\entity_value_inheritance\Entity\InheritanceInterface;

/**
 * Interface used for plugins.
 */
interface EntityValueInheritanceUpdaterPluginInterface extends DependentPluginInterface, PluginFormInterface {

  /**
   * Return the title of the current plugin.
   */
  public function getTitle(): string;

  /**
   * Return the plugin's description.
   */
  public function getDescription(): string;

  /**
   * Return the currently defined Inheritance entity.
   */
  public function getInheritance(): ?InheritanceInterface;

  /**
   * Set the Inheritance entity.
   */
  public function setInheritance(InheritanceInterface $inheritance): self;

  /**
   * Alter the entities.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Array of entities to alter.
   */
  public function entityLoad(EntityInterface $entity): void;

  /**
   * Alter the entity before saving.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to modify before saving.
   */
  public function preSaveEntity(EntityInterface $entity): void;

  /**
   * Do actions based on deleting the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity to modify before saving.
   */
  public function deleteEntity(EntityInterface $entity): void;

  /**
   * Update the destination entity with the provided information.
   *
   * @param \Drupal\Core\Entity\EntityInterface $sourceEntity
   *   Source Entity to get information from.
   * @param \Drupal\Core\Entity\EntityInterface $destinationEntity
   *   Destination Entity to update information for.
   *
   * @return bool
   *   Return TRUE to continue with updating or FALSE to stop
   */
  public function updateDestination(EntityInterface $sourceEntity, EntityInterface $destinationEntity): bool;

  /**
   * Alter the form.
   *
   * Allow plugins to alter the form if needed.
   *
   * @param array $form
   *   Form to modify.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form State Information.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current source entity that is being modified.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   Information about the field object itself.
   *
   * @return array
   *   Return the form.
   */
  public function alterForm(array $form, FormStateInterface $formState, EntityInterface $entity, FieldDefinitionInterface $fieldDefinition): array;

  /**
   * Alter the provided form field on the entity form.
   *
   * @param array $element
   *   Form element in a render array type format.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form State Information.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The current source entity that is being modified.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   Information about the field object itself.
   *
   * @return array
   *   Return the form element.
   */
  public function alterFormField(array $element, FormStateInterface $formState, EntityInterface $entity, FieldDefinitionInterface $fieldDefinition): array;

  /**
   * Returns the configuration form elements specific to this block plugin.
   *
   * Inheritance Plugins that need to add form elements to the normal
   * inheritance configuration form should implement this method.
   *
   * @param array $form
   *   The form definition array for the inheritance configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The renderable form array representing the entire configuration form.
   */
  public function inheritanceForm(array $form, FormStateInterface $form_state): array;

  /**
   * Adds inheritance type-specific validation for the inheritance plugin form.
   *
   * Note that this method takes the form structure and form state for the full
   * inheritance configuration form as arguments, not just the elements defined
   * in InheritanceInterface::inheritanceForm().
   *
   * @param array $form
   *   The form definition array for the full inheritance configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see \Drupal\entity_value_inheritance\Entity\InheritanceInterface::inheritanceForm()
   * @see \Drupal\entity_value_inheritance\Entity\InheritanceInterface::inheritanceSubmit()
   */
  public function inheritanceValidate(array $form, FormStateInterface $form_state): void;

  /**
   * Adds inheritance specific submission handling for the plugin form.
   *
   * Note that this method takes the form structure and form state for the full
   * inheritance configuration form as arguments, not just the elements defined
   * in InheritanceInterface::inheritanceForm().
   *
   * @param array $form
   *   The form definition array for the full block configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @see \Drupal\entity_value_inheritance\Entity\InheritanceInterface::inheritanceForm()
   * @see \Drupal\entity_value_inheritance\Entity\InheritanceInterface::inheritanceValidate()
   */
  public function inheritanceSubmit(array $form, FormStateInterface $form_state): void;

}
