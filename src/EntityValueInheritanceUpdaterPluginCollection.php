<?php

namespace Drupal\entity_value_inheritance;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultSingleLazyPluginCollection;

/**
 * Provides a collection of inheritance updater plugins.
 */
class EntityValueInheritanceUpdaterPluginCollection extends DefaultSingleLazyPluginCollection {

  /**
   * Constructs a new \Drupal\entity_value_inheritance\EntityValueInheritanceUpdaterPluginCollection object.
   *
   * @param \Drupal\Component\Plugin\PluginManagerInterface $manager
   *   The manager to be used for instantiating plugins.
   * @param string $instance_id
   *   The ID of the plugin instance.
   * @param array $configuration
   *   An array of configuration.
   * @param string $inheritanceId
   *   The unique ID of the inheritance entity using this plugin.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module Handler Service.
   */
  public function __construct(PluginManagerInterface $manager, $instance_id, array $configuration, protected string $inheritanceId, protected ModuleHandlerInterface $moduleHandler) {
    parent::__construct($manager, $instance_id, $configuration);
  }

  /**
   * {@inheritdoc}
   *
   * @return \Drupal\entity_value_inheritance\EntityValueInheritanceUpdaterPluginInterface
   *   Generated Plugin.
   */
  public function &get($instance_id) { // phpcs:ignore
    return parent::get($instance_id);
  }

  /**
   * {@inheritdoc}
   */
  protected function initializePlugin($instance_id) {
    try {
      parent::initializePlugin($instance_id);
    }
    catch (PluginException | PluginNotFoundException $e) {
      $module = $this->configuration['provider'];
      // Ignore inheritances belonging to uninstalled modules, but re-throw
      // valid exceptions when the module is installed and the plugin is
      // misconfigured.
      if (!$module || $this->moduleHandler->moduleExists($module)) {
        throw $e;
      }
    }
  }

}
