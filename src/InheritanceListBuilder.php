<?php

namespace Drupal\entity_value_inheritance;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\entity_value_inheritance\Services\Helper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a listing of inheritances.
 */
class InheritanceListBuilder extends ConfigEntityListBuilder {

  /**
   * Request Stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * Helper Service.
   *
   * @var \Drupal\entity_value_inheritance\Services\Helper
   */
  protected Helper $helper;

  /**
   * Form Builder Service.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected FormBuilderInterface $formBuilder;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $instance = parent::createInstance($container, $entity_type);
    $instance->requestStack = $container->get('request_stack');
    $instance->helper = $container->get('entity_value_inheritance.helper');
    $instance->formBuilder = $container->get('form_builder');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    $header = [];
    $header['label'] = $this->t('Label');
    $header['id'] = $this->t('Machine name');
    $header['status'] = $this->t('Status');
    $header['strategy'] = $this->t('Inherit Strategy');
    $header['source'] = $this->t('Source');
    $header['destination'] = $this->t('Destination');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    $row = [];
    /** @var \Drupal\entity_value_inheritance\Entity\InheritanceInterface $entity */
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['status'] = $entity->status() ? $this->t('Enabled') : $this->t('Disabled');
    $row['strategy'] = $this->helper->getStrategyLabel($entity->getPlugin());
    $row['source'] = [
      'data' => [
        '#theme' => 'item_list',
        '#items' => [
          $this->t('Entity Type: @entity', ['@entity' => $this->helper->getEntityTypeLabel($entity->getSourceEntityType())]),
          $this->t('Bundle Type: @bundle', ['@bundle' => $this->helper->getEntityBundleLabel($entity->getSourceEntityType(), $entity->getSourceBundle())]),
          $this->t('Field: @field', ['@field' => $this->helper->getEntityBundleFieldLabel($entity->getSourceEntityType(), $entity->getSourceBundle(), $entity->getSourceField())]),
        ],
      ],
    ];

    $row['destination'] = [
      'data' => [
        '#theme' => 'item_list',
        '#items' => [
          $this->t('Entity Type: @entity', ['@entity' => $this->helper->getEntityTypeLabel($entity->getDestinationEntityType())]),
          $this->t('Bundle Type: @bundle', ['@bundle' => $this->helper->getEntityBundleLabel($entity->getDestinationEntityType(), $entity->getDestinationBundle())]),
          $this->t('Field: @field', ['@field' => $this->helper->getEntityBundleFieldLabel($entity->getDestinationEntityType(), $entity->getDestinationBundle(), $entity->getDestinationField())]),
          $this->t('Reference Field: @field', ['@field' => $this->helper->getEntityBundleFieldLabel($entity->getDestinationEntityType(), $entity->getDestinationBundle(), $entity->getDestinationReferenceField())]),
        ],
      ],
    ];
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityListQuery(): QueryInterface {
    $query = $this->getStorage()->getQuery()
      ->accessCheck(TRUE)
      ->sort($this->entityType->getKey('id'));

    if (($source_entity_type = $this->requestStack->getCurrentRequest()->query->get('source_entity_type'))) {
      $query->condition('source_entity_type', $source_entity_type);
    }

    if (($destination_bundle_type = $this->requestStack->getCurrentRequest()->query->get('source_entity_bundle'))) {
      $query->condition('source_entity_bundle', $destination_bundle_type);
    }

    if (($destination_entity_type = $this->requestStack->getCurrentRequest()->query->get('destination_entity_type'))) {
      $query->condition('destination_entity_type', $destination_entity_type);
    }

    if (($destination_bundle_type = $this->requestStack->getCurrentRequest()->query->get('destination_entity_bundle'))) {
      $query->condition('destination_entity_bundle', $destination_bundle_type);
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function render(): array {
    $build['#attached']['library'][] = 'entity_value_inheritance/admin';
    $build['search_form'] = $this->formBuilder->getForm('Drupal\entity_value_inheritance\Form\InheritanceListBuilderForm');
    $build['list_builder_title']['#markup'] = '<h3>' . $this->t('Inheritances') . ':</h3>';
    $build['list_table'] = parent::render();
    return $build;
  }

}
