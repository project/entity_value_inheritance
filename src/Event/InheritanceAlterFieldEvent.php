<?php

namespace Drupal\entity_value_inheritance\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_value_inheritance\Entity\InheritanceInterface;

/**
 * Alter Field Event.
 *
 * Event is used for a module to alter the contents of the field element on the
 * end form for the user.
 */
class InheritanceAlterFieldEvent extends Event {

  /**
   * Constructs a new \Drupal\entity_value_inheritance\Event\InheritanceAlterFieldEvent object.
   *
   * @param array $element
   *   Element that can be altered.
   * @param string $message
   *   Initial message to set for field.
   * @param \Drupal\entity_value_inheritance\Entity\InheritanceInterface $inheritance
   *   Inheritance that is acting on the field.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   Entity that is being altered for the field.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $fieldDefinition
   *   Additional field information.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form State related to the current form and field.
   */
  public function __construct(protected array $element, protected string $message, protected InheritanceInterface $inheritance, protected EntityInterface $entity, protected FieldDefinitionInterface $fieldDefinition, protected FormStateInterface $formState) {

  }

  /**
   * Return the current form state.
   *
   * @return \Drupal\Core\Form\FormStateInterface
   *   Form State.
   */
  public function getFormState(): FormStateInterface {
    return $this->formState;
  }

  /**
   * Modify the current form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   Form State to replace the event with.
   *
   * @return self
   *   Return the current instance.
   */
  public function setFormState(FormStateInterface $formState): self {
    $this->formState = $formState;
    return $this;
  }

  /**
   * Return the element.
   *
   * @return array
   *   Form element.
   */
  public function getElement(): array {
    return $this->element;
  }

  /**
   * Modify the element and set it.
   *
   * @param array $element
   *   Element to replace with.
   *
   * @return self
   *   Return the current instance.
   */
  public function setElement(array $element): self {
    $this->element = $element;
    return $this;
  }

  /**
   * Return the message to use for the field.
   *
   * @return string
   *   Field Message.
   */
  public function getMessage(): string {
    return $this->message;
  }

  /**
   * Set the message to use for the field.
   *
   * @param string $message
   *   The message to set for the field.
   *
   * @return self
   *   Return the current instance.
   */
  public function setMessage(string $message): self {
    $this->message = $message;
    return $this;
  }

  /**
   * Return the source entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Source entity.
   */
  public function getEntity(): EntityInterface {
    return $this->entity;
  }

  /**
   * Return the Inheritance Config.
   *
   * @return \Drupal\entity_value_inheritance\Entity\InheritanceInterface
   *   Inheritance Config.
   */
  public function getInheritance(): InheritanceInterface {
    return $this->inheritance;
  }

  /**
   * Return the Field Definition.
   *
   * @return \Drupal\Core\Field\FieldDefinitionInterface
   *   Field Definition.
   */
  public function getFieldDefinition(): FieldDefinitionInterface {
    return $this->fieldDefinition;
  }

}
