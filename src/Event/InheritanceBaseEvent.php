<?php

namespace Drupal\entity_value_inheritance\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_value_inheritance\Entity\InheritanceInterface;

/**
 * Abstract class used for the base of events.
 */
abstract class InheritanceBaseEvent extends Event {

  /**
   * Constructs a new \Drupal\entity_value_inheritance\Event\InheritanceBaseEvent object.
   *
   * @param \Drupal\entity_value_inheritance\Entity\InheritanceInterface $inheritance
   *   Inheritance configuration.
   * @param \Drupal\Core\Entity\EntityInterface $sourceEntity
   *   Entity providing the information.
   * @param \Drupal\Core\Entity\EntityInterface $destinationEntity
   *   Entity being updated.
   */
  public function __construct(protected InheritanceInterface $inheritance, protected EntityInterface $sourceEntity, protected EntityInterface $destinationEntity) {

  }

  /**
   * Return the event's inheritance.
   *
   * @return \Drupal\entity_value_inheritance\Entity\InheritanceInterface
   *   Inheritance Configuration.
   */
  public function getInheritance(): InheritanceInterface {
    return $this->inheritance;
  }

  /**
   * Return the event's source entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Source Entity.
   */
  public function getSourceEntity(): EntityInterface {
    return $this->sourceEntity;
  }

  /**
   * Return the event's destination entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Source Entity.
   */
  public function getDestinationEntity(): EntityInterface {
    return $this->destinationEntity;
  }

}
