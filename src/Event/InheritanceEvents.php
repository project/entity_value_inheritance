<?php

namespace Drupal\entity_value_inheritance\Event;

/**
 * Defines the events that are used.
 */
final class InheritanceEvents {

  /**
   * Event name use at start of the update process.
   */
  const START_UPDATE = 'entity_value_inheritance.start_update';

  /**
   * Name of the event fired before the updateDestination hook is ran.
   */
  const PRE_UPDATE = 'entity_value_inheritance.pre_update';

  /**
   * Event used for altering the update list.
   */
  const ALTER_UPDATE_LIST = 'entity_value_inheritance.alter_update_list';

  /**
   * Event used for altering the field.
   */
  const ALTER_FIELD = 'entity_value_inheritance.alter_field';

  /**
   * Name of the event fired after the updateDestination hook is ran.
   */
  const POST_UPDATE = 'entity_value_inheritance.post_update';

  /**
   * Event name used for running the save entity functionality.
   */
  const SAVE_ENTITIES = 'entity_value_inheritance.save';

}
