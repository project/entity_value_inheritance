<?php

namespace Drupal\entity_value_inheritance\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Entity\EntityInterface;

/**
 * Event used to alter the list of events.
 */
class InheritanceAlterUpdateListEvent extends Event {

  /**
   * Constructs a new \Drupal\entity_value_inheritance\Event\InheritanceAlterUpdateListEvent object.
   *
   * @param \Drupal\Core\Entity\EntityInterface $sourceEntity
   *   Source Entity that will be used as the source.
   * @param \Drupal\Core\Entity\EntityInterface[] $destinationList
   *   List of entities to act on.
   */
  public function __construct(protected EntityInterface $sourceEntity, protected array $destinationList) {

  }

  /**
   * Get the source entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Return the source entity.
   */
  public function getSourceEntity(): EntityInterface {
    return $this->sourceEntity;
  }

  /**
   * Return the list of entities for destination.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Return list of entities.
   */
  public function getList(): array {
    return $this->destinationList;
  }

  /**
   * Set the list of destination entities.
   *
   * @param \Drupal\Core\Entity\EntityInterface[] $list
   *   The list of new entities to replace with.
   *
   * @return \Drupal\entity_value_inheritance\Event\InheritanceAlterUpdateListEvent
   *   Return the current class;
   */
  public function setList(array $list): self {
    $this->destinationList = $list;
    return $this;
  }

}
