<?php

namespace Drupal\entity_value_inheritance\Event;

/**
 * Event used for running post update functionality.
 */
class InheritancePostUpdateEvent extends InheritanceBaseEvent {

}
