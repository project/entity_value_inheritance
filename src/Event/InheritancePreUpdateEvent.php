<?php

namespace Drupal\entity_value_inheritance\Event;

/**
 * Event used for pre update functionality.
 */
class InheritancePreUpdateEvent extends InheritanceBaseEvent {

  /**
   * Continue Process Value.
   */
  protected bool $continueProcess = TRUE;

  /**
   * Return if the process can continue forward.
   */
  public function canContinue(): bool {
    return $this->continueProcess;
  }

  /**
   * Stop the update from happening.
   */
  public function stopUpdate(): self {
    $this->continueProcess = FALSE;
    return $this;
  }

}
