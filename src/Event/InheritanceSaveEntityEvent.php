<?php

namespace Drupal\entity_value_inheritance\Event;

use Drupal\Component\EventDispatcher\Event;

/**
 * Event used for running save entity functionality.
 */
class InheritanceSaveEntityEvent extends Event {

  /**
   * Constructs a new \Drupal\entity_value_inheritance\Event\InheritanceSaveEntityEvent object.
   *
   * @param \Drupal\entity_value_inheritance\Entity\InheritanceInterface[] $inheritances
   *   List of Inheritances being acted on.
   * @param \Drupal\Core\Entity\EntityInterface[] $destinationEntities
   *   List of Entities to be acted on.
   */
  public function __construct(protected array $inheritances = [], protected array $destinationEntities = []) {

  }

  /**
   * Return a list of all destination entities.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   List of destination entities.
   */
  public function getDestinationEntities(): array {
    return $this->destinationEntities;
  }

  /**
   * Return a list of all inheritances.
   *
   * @return \Drupal\entity_value_inheritance\Entity\InheritanceInterface[]
   *   List of Inheritances.
   */
  public function getInheritances(): array {
    return $this->inheritances;
  }

}
