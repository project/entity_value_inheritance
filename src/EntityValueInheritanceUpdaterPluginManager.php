<?php

namespace Drupal\entity_value_inheritance;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\entity_value_inheritance\Annotation\EntityValueInheritanceUpdater;

/**
 * A plugin manager for the EntityValueInheritanceUpdater plugins.
 */
class EntityValueInheritanceUpdaterPluginManager extends DefaultPluginManager {

  /**
   * Track of all created instances.
   *
   * @var array
   */
  protected array $instances = [];

  /**
   * Constructs a new \Drupal\entity_value_inheritance\EntityValueInheritanceUpdaterPluginManager object.
   *
   * @param \Traversable $namespaces
   *   Namespaces to search.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   Cache Backend Service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   Module Handler Service.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cacheBackend, ModuleHandlerInterface $moduleHandler) {
    parent::__construct(
      'Plugin/EntityValueInheritanceUpdater',
      $namespaces,
      $moduleHandler,
      EntityValueInheritanceUpdaterPluginInterface::class,
      EntityValueInheritanceUpdater::class
    );

    $this->alterInfo('entity_value_inheritance_updater_info');

    $this->setCacheBackend($cacheBackend, 'entity_value_inheritance_updater_info', ['entity_value_inheritance_updater_info']);
  }

}
