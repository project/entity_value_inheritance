<?php

namespace Drupal\entity_value_inheritance\EventSubscriber;

use Drupal\Core\Entity\EntityStorageException;
use Drupal\entity_value_inheritance\Event\InheritanceEvents;
use Drupal\entity_value_inheritance\Event\InheritanceSaveEntityEvent;

/**
 * Event Subscriber to save entities.
 */
class SaveEntitySubscriber extends EventSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events = [];
    $events[InheritanceEvents::SAVE_ENTITIES][] = ['saveEntities', 1000];
    return $events;
  }

  /**
   * Save the entities that have been altered.
   *
   * @param \Drupal\entity_value_inheritance\Event\InheritanceSaveEntityEvent $event
   *   Event with the entities and inheritances.
   */
  public function saveEntities(InheritanceSaveEntityEvent $event) {
    $updateEntities = $event->getDestinationEntities();

    foreach ($updateEntities as $updateEntity) {
      // If the entity has been altered then do a save.
      if (isset($updateEntity->altered) && $updateEntity->altered === TRUE) {
        try {
          $updateEntity->save();
        }
        catch (EntityStorageException | \Exception $exception) {
          $this->helper->logger()->error($exception->getMessage());
        }
      }
    }
  }

}
