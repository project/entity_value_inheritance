<?php

namespace Drupal\entity_value_inheritance\EventSubscriber;

use Drupal\entity_value_inheritance\Services\Helper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Abstract class.
 */
abstract class EventSubscriberBase implements EventSubscriberInterface {

  /**
   * Constructs a new \Drupal\entity_value_inheritance\EventSubscriber\EventSubscriberBase object.
   */
  public function __construct(protected Helper $helper) {

  }

}
