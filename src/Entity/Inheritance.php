<?php

namespace Drupal\entity_value_inheritance\Entity;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\entity_value_inheritance\EntityValueInheritanceUpdaterPluginCollection;
use Drupal\entity_value_inheritance\EntityValueInheritanceUpdaterPluginManager;

/**
 * Defines the inheritance entity type.
 *
 * @ConfigEntityType(
 *   id = "inheritance",
 *   label = @Translation("Inheritance"),
 *   label_collection = @Translation("Inheritances"),
 *   label_singular = @Translation("inheritance"),
 *   label_plural = @Translation("inheritances"),
 *   label_count = @PluralTranslation(
 *     singular = "@count inheritance",
 *     plural = "@count inheritances",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\entity_value_inheritance\InheritanceListBuilder",
 *     "form" = {
 *       "add" = "Drupal\entity_value_inheritance\Form\InheritanceForm",
 *       "edit" = "Drupal\entity_value_inheritance\Form\InheritanceForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "inheritance",
 *   admin_permission = "administer inheritance",
 *   links = {
 *     "collection" = "/admin/structure/inheritance",
 *     "add-form" = "/admin/structure/inheritance/add",
 *     "edit-form" = "/admin/structure/inheritance/{inheritance}",
 *     "delete-form" = "/admin/structure/inheritance/{inheritance}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "strategy",
 *     "settings",
 *     "source_entity_type",
 *     "source_entity_bundle",
 *     "source_entity_field",
 *     "destination_entity_type",
 *     "destination_entity_bundle",
 *     "destination_entity_field",
 *     "destination_entity_referencing_field"
 *   }
 * )
 */
class Inheritance extends ConfigEntityBase implements InheritanceInterface {

  /**
   * The inheritance ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The inheritance label.
   *
   * @var string
   */
  protected $label;

  /**
   * The plugin instance settings.
   *
   * @var array
   */
  protected array $settings = [];

  /**
   * The inheritance status.
   *
   * @var bool
   */
  protected $status = TRUE;

  /**
   * The inheritance description.
   *
   * @var string
   */
  protected string $description = '';

  /**
   * The strategy type for using when updating destination.
   *
   * @var string
   */
  protected string $strategy = 'update';

  /**
   * The plugin collection that holds the block plugin for this entity.
   *
   * @var \Drupal\entity_value_inheritance\EntityValueInheritanceUpdaterPluginCollection
   */
  protected $pluginCollection;

  /**
   * The source entity type id.
   *
   * @var string|null
   */
  protected ?string $source_entity_type = NULL;

  /**
   * The source entity bundle type.
   *
   * @var string|null
   */
  protected ?string $source_entity_bundle = NULL;

  /**
   * The source entity field to copy.
   *
   * @var string|null
   */
  protected ?string $source_entity_field = NULL;

  /**
   * The destination entity type id.
   *
   * @var string|null
   */
  protected ?string $destination_entity_type = NULL;

  /**
   * The destination entity bundle type.
   *
   * @var string|null
   */
  protected ?string $destination_entity_bundle = NULL;

  /**
   * The destination entity field.
   *
   * @var string|null
   */
  protected ?string $destination_entity_field = NULL;

  /**
   * The destination entity reference field that link to the source.
   *
   * @var string|null
   */
  protected ?string $destination_entity_referencing_field = NULL;

  /**
   * Plugin Manager Service.
   *
   * @var \Drupal\entity_value_inheritance\EntityValueInheritanceUpdaterPluginManager
   */
  protected ?EntityValueInheritanceUpdaterPluginManager $pluginManager = NULL;

  /**
   * {@inheritdoc}
   */
  public function getPlugin() {
    return $this->getPluginCollection()->get($this->strategy);
  }

  /**
   * Encapsulates the creation of the inheritance's LazyPluginCollection.
   *
   * @return \Drupal\Component\Plugin\LazyPluginCollection
   *   The inheritance's plugin collection.
   */
  protected function getPluginCollection() {
    if (!$this->pluginCollection) {
      $this->pluginCollection = new EntityValueInheritanceUpdaterPluginCollection(
        $this->getPluginManager(),
        $this->strategy,
        $this->get('settings'),
        $this->id() ?? '',
        $this->moduleHandler()
      );
    }
    return $this->pluginCollection;
  }

  /**
   * Return the Inheritance Updater Service.
   *
   * @return \Drupal\Component\Plugin\PluginManagerInterface
   *   Plugin Manager Service.
   */
  protected function getPluginManager(): PluginManagerInterface {
    if (!$this->pluginManager) {
      $this->pluginManager = \Drupal::service('plugin.manager.entity_value_inheritance_updater');
    }
    return $this->pluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() : string {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceEntityType(): ?string {
    return $this->source_entity_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceBundle(): ?string {
    return $this->source_entity_bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceField(): ?string {
    return $this->source_entity_field;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationEntityType(): ?string {
    return $this->destination_entity_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationBundle(): ?string {
    return $this->destination_entity_bundle;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationField(): ?string {
    return $this->destination_entity_field;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinationReferenceField() : ?string {
    return $this->destination_entity_referencing_field;
  }

  /**
   * {@inheritdoc}
   */
  public function getStrategy(): ?string {
    return $this->strategy;
  }

  /**
   * {@inheritdoc}
   */
  public function getSettings(): array {
    return $this->settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getSetting(string $key): mixed {
    return $this->settings[$key] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();

    $this->addDependency('module', $this->settings['provider']);

    foreach ([[
      'type' => $this->source_entity_type,
      'bundle' => $this->source_entity_bundle,
      'field' => $this->source_entity_field,
    ], [
      'type' => $this->destination_entity_type,
      'bundle' => $this->destination_entity_bundle,
      'field' => $this->destination_entity_field,
      'reference' => $this->destination_entity_referencing_field,
    ],
    ] as $config
    ) {
      $this->addDependency('config',
        sprintf(
          'field.field.%s.%s.%s',
          $config['type'],
          $config['bundle'],
          $config['field']
        )
      );

      if (isset($config['reference'])) {
        $this->addDependency('config',
          sprintf(
            'field.field.%s.%s.%s',
            $config['type'],
            $config['bundle'],
            $config['reference']
          )
        );
      }
    }

    return $this;
  }

}
