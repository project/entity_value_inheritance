<?php

namespace Drupal\entity_value_inheritance\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an inheritance entity type.
 */
interface InheritanceInterface extends ConfigEntityInterface {

  /**
   * Returns the plugin instance.
   *
   * @return \Drupal\entity_value_inheritance\EntityValueInheritanceUpdaterPluginInterface
   *   The plugin instance for this inheritance.
   */
  public function getPlugin();

  /**
   * Return the description for the field.
   */
  public function getDescription(): string;

  /**
   * Get the source entity type.
   */
  public function getSourceEntityType(): ?string;

  /**
   * Get the source bundle type.
   */
  public function getSourceBundle(): ?string;

  /**
   * Get the source field.
   */
  public function getSourceField(): ?string;

  /**
   * Get the destination entity.
   */
  public function getDestinationEntityType(): ?string;

  /**
   * Get the destination bundle.
   */
  public function getDestinationBundle(): ?string;

  /**
   * Get the destination field.
   */
  public function getDestinationField(): ?string;

  /**
   * Get the destination referencing field to use.
   */
  public function getDestinationReferenceField(): ?string;

  /**
   * Get the update strategy.
   */
  public function getStrategy(): ?string;

  /**
   * Return the settings for the plugin.
   */
  public function getSettings(): array;

  /**
   * Return the specific setting item.
   */
  public function getSetting(string $key): mixed;

}
