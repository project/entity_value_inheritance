<?php

namespace Drupal\entity_value_inheritance\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an EntityValueInheritanceUpdater annotation object.
 *
 * @Annotation
 */
class EntityValueInheritanceUpdater extends Plugin {

  /**
   * A brief title used to define the plugin.
   *
   * @var string
   */
  public string $title;

  /**
   * A description of the plugin and it's purpose.
   *
   * @var string
   */
  public string $description;

}
