<?php

namespace Drupal\entity_value_inheritance\Plugin\Action;

use Drupal\Core\Action\Plugin\Action\DeleteAction;

/**
 * Redirects to a entity_value_inheritance deletion form.
 *
 * @Action(
 *   id = "entity_value_inheritance_delete_action",
 *   label = @Translation("Delete Inheritances"),
 *   type = "inheritance",
 *   confirm_form_route_name = "entity.inheritance.multiple_delete_confirm"
 * )
 */
class InheritanceDeleteAction extends DeleteAction {}
