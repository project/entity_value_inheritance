<?php

namespace Drupal\entity_value_inheritance\Plugin\EntityValueInheritanceUpdater;

use Drupal\Core\Entity\EntityInterface;
use Drupal\entity_value_inheritance\EntityValueInheritanceUpdaterPluginBase;

/**
 * Update the data if the destination matches the original source.
 *
 * @EntityValueInheritanceUpdater (
 *   id = "update",
 *   title = @Translation("Update"),
 *   description = @Translation("Sync data only if the destination matches the source."),
 * )
 */
class InheritanceUpdatePlugin extends EntityValueInheritanceUpdaterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function updateDestination(EntityInterface $sourceEntity, EntityInterface $destinationEntity): bool {
    $sourceField = $sourceEntity?->original?->get($this->inheritance->get('source_entity_field'));
    $destinationField = $destinationEntity->get($this->inheritance->get('destination_entity_field'));
    if (
      !($sourceField->isEmpty() && $destinationField->isEmpty()) &&
      $sourceField->getValue() !== $destinationField->getValue()
    ) {
      return FALSE;
    }

    return parent::updateDestination($sourceEntity, $destinationEntity);
  }

}
