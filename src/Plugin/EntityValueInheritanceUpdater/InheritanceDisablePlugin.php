<?php

namespace Drupal\entity_value_inheritance\Plugin\EntityValueInheritanceUpdater;

use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_value_inheritance\EntityValueInheritanceUpdaterPluginBase;

/**
 * Disable the field and overwrite the data on the destinations value.
 *
 * @EntityValueInheritanceUpdater(
 *   id = "disable",
 *   title = @Translation("Disable"),
 *   description = @Translation("Disable and overwrite the field value."),
 * )
 */
class InheritanceDisablePlugin extends EntityValueInheritanceUpdaterPluginBase {

  /**
   * {@inheritdoc}
   */
  public function inheritanceForm(array $form, FormStateInterface $form_state): array {
    $form = parent::inheritanceForm($form, $form_state);

    $form['message'] = [
      '#title' => $this->t('Disable Change Message'),
      '#description' => $this->t('Message used when displaying to the end user.'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['message'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['message'] = '';
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function inheritanceValidate(array $form, FormStateInterface $form_state): void {
    parent::inheritanceValidate($form, $form_state);
    $this->configuration['message'] = $form_state->getValue('message');
  }

}
