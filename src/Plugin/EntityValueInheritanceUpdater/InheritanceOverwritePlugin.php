<?php

namespace Drupal\entity_value_inheritance\Plugin\EntityValueInheritanceUpdater;

use Drupal\entity_value_inheritance\EntityValueInheritanceUpdaterPluginBase;

/**
 * Overwrite the data regardless of the destinations value.
 *
 * @EntityValueInheritanceUpdater(
 *   id = "overwrite",
 *   title = @Translation("Overwrite"),
 *   description = @Translation("Update the destination regardless of it's current value."),
 * )
 */
class InheritanceOverwritePlugin extends EntityValueInheritanceUpdaterPluginBase {

}
