<?php

/**
 * @file
 * Custom hooks exposed by the entity_value_inheritance module.
 */

/**
 * Alter the Field Type List.
 *
 * @param string[] $list
 *   List of field types.
 */
function hook_entity_value_inheritance_field_type_list_alter(array &$list) {
  unset($list['string']);
}

/**
 * Alter the list of entity types.
 *
 * @param array $list
 *   List of entities.
 */
function hook_entity_value_inheritance_entity_type_list_alter(array &$list) {
  unset($list['node']);
}

/**
 * Alter the list of bundles for entity type.
 *
 * @param array $list
 *   List of entities.
 * @param string $entityTypeId
 *   Entity Type ID.
 */
function hook_entity_value_inheritance_entity_bundle_list_alter(array &$list, string $entityTypeId) {
  if (isset($list['foo'])) {
    $list['foo']['label'] = t('Bar');
  }
}

/**
 * Alter the list of bundles for entity type.
 *
 * @param array $list
 *   List of entities.
 * @param string $entityTypeId
 *   Entity Type ID.
 */
function hook_entity_value_inheritance_entity_bundle_list__ENTITY_TYPE_alter(array &$list, string $entityTypeId) {
  if (isset($list['foo'])) {
    $list['foo']['label'] = t('Bar');
  }
}

/**
 * Alter the fields for the entity type and bundle.
 *
 * @param \Drupal\Core\Field\FieldDefinitionInterface[] $list
 *   List of fields keyed by field name.
 * @param string $entityTypeId
 *   Entity Type ID.
 * @param string $entityBundleId
 *   Entity Bundle ID.
 */
function hook_entity_value_inheritance_entity_bundle_field_list_alter(array &$list, string $entityTypeId, string $entityBundleId) {
  unset($list['foo']);
}

/**
 * Alter the fields for the entity type and bundle.
 *
 * @param \Drupal\Core\Field\FieldDefinitionInterface[] $list
 *   List of fields keyed by field name.
 * @param string $entityTypeId
 *   Entity Type ID.
 * @param string $entityBundleId
 *   Entity Bundle ID.
 */
function hook_entity_value_inheritance_entity_bundle_field_list__ENTITY_TYPE_alter(array &$list, string $entityTypeId, string $entityBundleId) {

}

/**
 * Alter the fields for the entity type and bundle.
 *
 * @param \Drupal\Core\Field\FieldDefinitionInterface $list
 *   List of fields keyed by field name.
 * @param string $entityTypeId
 *   Entity Type ID.
 * @param string $entityBundleId
 *   Entity Bundle ID.
 */
function hook_entity_value_inheritance_entity_bundle_field_list__ENTITY_TYPE__BUNDLE_alter(array &$list, string $entityTypeId, string $entityBundleId) {

}
