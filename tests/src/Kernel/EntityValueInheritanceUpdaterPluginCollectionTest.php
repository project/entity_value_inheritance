<?php

namespace Drupal\Tests\entity_value_inheritance\Kernel;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\entity_value_inheritance\EntityValueInheritanceUpdaterPluginBase;
use Drupal\entity_value_inheritance\EntityValueInheritanceUpdaterPluginCollection;
use Drupal\entity_value_inheritance\EntityValueInheritanceUpdaterPluginInterface;

/**
 * Test Class for the Plugin Collection.
 */
class EntityValueInheritanceUpdaterPluginCollectionTest extends EntityValueInheritanceTestBase {

  /**
   * Create a mock handler for help.
   *
   * @return \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected function getMockModuleHandler(): ModuleHandlerInterface
  {
    $mockModuleHandler = $this->createMock(ModuleHandlerInterface::class);
    $mockModuleHandler
      ->expects($this->any())
      ->method('moduleExists')
      ->willReturn(true);

    return $mockModuleHandler;
  }

  /**
   * Test Creation of Plugin Collection.
   */
  public function testEntityValueInheritanceUpdaterPluginCollectionCreation(): void {

    $pluginManager = $this->container->get('plugin.manager.entity_value_inheritance_updater');

    $collection = new EntityValueInheritanceUpdaterPluginCollection(
      $pluginManager,
      'disable',
      ['provider' => 'entity_value_inheritance'],
      'bar',
      $this->getMockModuleHandler()
    );
    $plugin = $collection->get('disable');

    $this->assertInstanceOf(EntityValueInheritanceUpdaterPluginBase::class, $plugin);
  }

  /**
   * Test that Plugin Not Found Exception.
   */
  public function testEntityValueInheritanceUpdaterPluginCollectionPluginNotFoundException(): void {

    $pluginManager = $this->container->get('plugin.manager.entity_value_inheritance_updater');

    $this->expectException(PluginNotFoundException::class);
    new EntityValueInheritanceUpdaterPluginCollection(
      $pluginManager,
      'foo',
      ['provider' => 'entity_value_inheritance'],
      'bar',
      $this->getMockModuleHandler()
    );
  }

  /**
   * Test plugin not found exception when a different plugin id is used.
   */
  public function testEntityValueInheritanceUpdaterPluginCollectionPluginException(): void {

    $pluginManager = $this->container->get('plugin.manager.entity_value_inheritance_updater');

    $collection = new EntityValueInheritanceUpdaterPluginCollection(
      $pluginManager,
      'disable',
      ['provider' => 'entity_value_inheritance'],
      'bar',
      $this->getMockModuleHandler()
    );

    $this->expectException(PluginNotFoundException::class);
    $collection->get('foo');
  }

  /**
   * Test actual instance is returned matching interface.
   */
  public function testEntityValueInheritanceUpdaterPluginCollectionSuccess(): void {
    $pluginManager = $this->container->get('plugin.manager.entity_value_inheritance_updater');
    $collection = new EntityValueInheritanceUpdaterPluginCollection(
      $pluginManager,
      'disable',
      ['provider' => 'entity_value_inheritance'],
      'bar',
      $this->getMockModuleHandler()
    );
    $item = $collection->get('disable');
    $this->assertInstanceOf(EntityValueInheritanceUpdaterPluginInterface::class, $item);
  }

}
