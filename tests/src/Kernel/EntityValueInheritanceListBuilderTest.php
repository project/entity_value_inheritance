<?php

namespace Drupal\Tests\entity_value_inheritance\Kernel;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\entity_value_inheritance\InheritanceListBuilder;

/**
 * Testing the List Builder.
 */
class EntityValueInheritanceListBuilderTest extends EntityValueInheritanceTestBase {

  use StringTranslationTrait;

  /**
   * Test List Builder.
   */
  public function testListBuilder(): void {
    $entityTypes = $this->container->get('entity_type.manager')->getDefinitions();
    $entityType = $entityTypes['inheritance'];

    $listBuilder = InheritanceListBuilder::createInstance($this->container, $entityType);

    /** @var \Drupal\entity_value_inheritance\Entity\Inheritance $entity */
    $entity = $this->container->get('entity_type.manager')->getStorage('inheritance')->load('source');

    $header = $listBuilder->buildHeader();
    $row = $listBuilder->buildRow($entity);

    $this->assertIsArray($header);
    $this->assertIsArray($row);

    $this->assertArrayHasKey('label', $header);
    $this->assertArrayHasKey('id', $header);
    $this->assertArrayHasKey('status', $header);
    $this->assertArrayHasKey('strategy', $header);
    $this->assertArrayHasKey('source', $header);
    $this->assertArrayHasKey('destination', $header);

    $this->assertEquals($entity->label(), $row['label']);
    $this->assertEquals($entity->id(), $row['id']);
    $this->assertIsBool($entity->status());
    $this->assertEquals($this->t('Enabled'), $row['status']);
  }

}
