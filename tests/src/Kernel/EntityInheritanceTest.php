<?php

namespace Drupal\Tests\entity_value_inheritance\Kernel;

use Drupal\entity_value_inheritance\Entity\InheritanceInterface;
use Drupal\entity_value_inheritance\EntityValueInheritanceUpdaterPluginInterface;

/**
 * Testing of Entity Class.
 *
 * @group entity_value_inheritance
 */
class EntityInheritanceTest extends EntityValueInheritanceTestBase {

  /**
   * Test all functions for entity class.
   */
  public function testInheritanceEntity(): void {
    $entityManager = $this->container->get('entity_type.manager')->getStorage('inheritance');

    /** @var \Drupal\entity_value_inheritance\Entity\InheritanceInterface $entity */
    $entity = $entityManager->load('source');

    $this->assertInstanceOf(InheritanceInterface::class, $entity);
    $this->assertEquals('disable', $entity->getStrategy());
    $this->assertEquals('entity_value_inheritance', $entity->getSetting('provider'));
    $settings = $entity->getSettings();
    $this->assertIsArray($settings);
    $this->assertArrayHasKey('provider', $settings);

    $this->assertEquals('node', $entity->getSourceEntityType());
    $this->assertEquals('source', $entity->getSourceBundle());
    $this->assertEquals('field_source_field', $entity->getSourceField());
    $this->assertEquals('node', $entity->getDestinationEntityType());
    $this->assertEquals('destination', $entity->getDestinationBundle());
    $this->assertEquals('field_destination_field', $entity->getDestinationField());
    $this->assertEquals('field_source', $entity->getDestinationReferenceField());

    $plugin = $entity->getPlugin();
    $this->assertInstanceOf(EntityValueInheritanceUpdaterPluginInterface::class, $plugin);

    $this->assertEquals('disable', $entity->getStrategy());
    $this->assertEquals('Sample inheritance used for testing purposes.', $entity->getDescription());
  }

}
