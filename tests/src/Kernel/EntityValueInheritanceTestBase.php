<?php

namespace Drupal\Tests\entity_value_inheritance\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test base used for Kernel Tests.
 *
 * @group entity_value_inheritance
 */
abstract class EntityValueInheritanceTestBase extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'system', 'field', 'text', 'user', 'node', 'path', 'entity_value_inheritance', 'entity_value_inheritance_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installEntitySchema('inheritance');

    $this->installSchema('node', ['node_access']);
    $this->installConfig(['system', 'node', 'user', 'entity_value_inheritance', 'entity_value_inheritance_test']);
    $this->installSchema('user', ['users_data']);
  }

}
