<?php

namespace Drupal\Tests\entity_value_inheritance\Functional;

use Drupal\Core\Url;

/**
 * Used for testing list builder.
 *
 * @group entity_value_inheritance
 */
class InheritListBuilderTest extends EntityValueInheritanceTestBase {

  /**
   * Test that list builder returns 403 for users who don't have access.
   */
  public function testListBuilderAccessDenied(): void {
    $this->drupalLogout();
    $this->user = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($this->user);
    $this->drupalGet(Url::fromRoute('entity.inheritance.collection'));
    $this->assertSession()->statusCodeEquals(403);
  }

  /**
   * Test successful seeing collection page.
   */
  public function testListBuilderSuccess(): void {
    $this->drupalGet(Url::fromRoute('entity.inheritance.collection'));
    $this->assertSession()->statusCodeEquals(200);
  }

}
