<?php

namespace Drupal\Tests\entity_value_inheritance\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Entity Value Test Base.
 */
abstract class EntityValueInheritanceTestBase extends BrowserTestBase {

  /**
   * Set the default theme to use.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['entity_value_inheritance', 'entity_value_inheritance_test'];

  /**
   * A user with permission to administer site configuration.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->user = $this->drupalCreateUser(['administer site configuration', 'administer inheritance']);
    $this->drupalLogin($this->user);
  }

}
