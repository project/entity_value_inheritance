<?php

namespace Drupal\Tests\entity_value_inheritance\Functional;

use Drupal\Core\Url;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group entity_value_inheritance
 */
class LoadTest extends EntityValueInheritanceTestBase {

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testLoad() {
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->assertSession()->statusCodeEquals(200);
  }

}
