<?php

/**
 * @file
 * Contains entity_value_inheritance.module.
 */

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function entity_value_inheritance_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.entity_value_inheritance':
      $text = file_get_contents(dirname(__FILE__) . "/README.md");
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . $text . '</pre>';
      }
      else {
        // Use the Markdown filter to render the README.
        $filter_manager = \Drupal::service('plugin.manager.filter');
        $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
        $config = ['settings' => $settings];
        $filter = $filter_manager->createInstance('markdown', $config);
        return $filter->process($text, 'en');
      }
  }
  return NULL;

}

/**
 * Implements hook_entity_update().
 */
function entity_value_inheritance_entity_update(EntityInterface $sourceEntity) {
  \Drupal::service('entity_value_inheritance.updater')->updateDestination($sourceEntity);
}

/**
 * Implements hook_entity_insert().
 */
function entity_value_inheritance_entity_insert(EntityInterface $sourceEntity) {
  \Drupal::service('entity_value_inheritance.updater')->insertDestination($sourceEntity);
}

/**
 * Implements hook_form_alter().
 */
function entity_value_inheritance_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  \Drupal::service('entity_value_inheritance.updater')->alterForm($form, $form_state);
}

/**
 * Implements hook_entity_presave().
 */
function entity_value_inheritance_entity_presave(EntityInterface $entity) {
  \Drupal::service('entity_value_inheritance.updater')->preSaveEntity($entity);
}

/**
 * Implements hook_entity_delete().
 */
function entity_value_inheritance_entity_delete(EntityInterface $entity) {
  \Drupal::service('entity_value_inheritance.updater')->deleteEntity($entity);
}

/**
 * Implements hook_entity_load().
 */
function entity_value_inheritance_entity_load(array $entities, $entity_type_id) {
  \Drupal::service('entity_value_inheritance.updater')->entityLoad($entities, $entity_type_id);
}
